-- 1 2 -> [(1, 2), (2, 4), (3, 6) ... ]
indices down right = scanl1 (\(px, py) (x, y) -> (px+x, py+y)) (repeat (down, right))

comp :: Int -> Int -> [String] -> Int
comp down right lns = foldl (\acc (i, j) ->
      let ln = (cycle (lns !! i)) in
          if ln !! j == '#'
          then acc + 1
          else acc
      ) 0 ix
      where
        ix = take (length lns `div` down - 1) (indices down right)

main = do
     input <- readFile "input"
     let lns = lines input
     putStrLn $ show (comp 1 3 lns) -- part 1

     putStrLn $ show ((comp 1 1 lns) * (comp 1 3 lns) * (comp 1 5 lns) * (comp 1 7 lns) * (comp 2 1 lns)) -- part 2
