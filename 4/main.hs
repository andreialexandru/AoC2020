import Data.Char
import Data.List

example_data = lines "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm\n\
\iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884 hcl:#cfa07d byr:1929\n\
\hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm\n\
\hcl:#cfa07d eyr:2025 pid:166559648 iyr:2011 ecl:brn hgt:59in"


all_fields = ["ecl","pid","eyr","hcl","byr","iyr","cid","hgt"]
all_fields' = ["ecl","pid","eyr","hcl","byr","iyr","hgt"]

getFields = map (take 3) . words

part1 lns = foldl (
  \acc fs ->
    if all_fields' \\ fs == []
    then acc+1
    else acc
  ) 0 (map getFields lns)


-----

splitOne str = (take 3 pref, suf)
  where (pref, suf) = splitAt 4 str

splitAll = map splitOne . words

part1' = filter (\p -> all_fields' \\ (map fst p) == []) . (map splitAll)

check_year l h val = case (reads val) of
  [(y, [])] -> y >= l && y <= h
  _       -> False

check_height val = case unit of
  "cm" -> h >= 150 && h <= 193
  "in" -> h >= 59 && h <= 76
  _ -> False
  where
    (pref, unit) = span isDigit val
    h = read pref

check_col ('#':val)
  | length val == 6 = all (\ch -> isDigit ch || (ch >= 'a' && ch <= 'f')) val
  | otherwise       = False
check_col _ = False

check_eye val = elem val ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

check_passport val = length val == 9 && all isDigit val

validator (field, val)
  | field == "byr" = check_year 1920 2002 val 
  | field == "iyr" = check_year 2010 2020 val 
  | field == "eyr" = check_year 2020 2030 val 
  | field == "hgt" = check_height val
  | field == "hcl" = check_col val
  | field == "ecl" = check_eye val
  | field == "pid" = check_passport val
  | field == "cid" = True

part2 = filter (all validator) . part1'


main = do
  input <- readFile "input_p" -- processed input, lines merged
  let lns = lines input
  putStrLn $ show (part1 lns)
  putStrLn $ show (length . part1' $ lns)

  putStrLn $ show (length . part2 $ lns)
