
#   Replace F,L with 0 and R,B with 1    Asc            Convert from binary to decimal                       Last # is Part1    Generate asc numbers from first to last seat          Difference between our input and seq is answer to Part2
sed -e "s/[F,L]/0/g;s/[R,B]/1/g" input | sort | xargs -I {} -n1 sh -c "echo \"ibase=2; {}\" | bc" > dec_in; tail -n 1 dec_in; seq $(head -n 1 dec_in) $(tail -n 1 dec_in) > seq_in; comm -13 dec_in seq_in; rm dec_in seq_in
