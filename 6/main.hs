import Data.List


foo :: [String] -> Int
foo ln = length (foldl1 (\acc x -> intersect acc x) ln)

part2 :: [[String]] -> Int
part2 lns = sum (map (foo) lns)

main = do
  input <- readFile "input_p2"
  let lns = map words (lines input)
  putStrLn $ show (part2 lns)
